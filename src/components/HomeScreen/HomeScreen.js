import React, { Component } from 'react';
import {
    View, Text, TouchableOpacity, Image
} from 'react-native';
import styles from './HomeScreenStyle';


export default class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
           
        }
    }

    render() {
        return (
            <View>

                <TouchableOpacity>

                    <View style={styles.menuViewStyles}>
                        <Image style={styles.menuViewImageStyles}
                            source={require('../../assets/menu.png')}
                        />

                    </View>

                </TouchableOpacity>

                <Text style={styles.mainTextStyles}>Choose Your Best</Text>
                <Text style={styles.subText1Styles}>Choose Your Best</Text>

                <View>

                    <Image style={styles.chair2ImageStyles}
                        source={require('../../assets/Chair2.jpg')}
                    />

                    <Image style={styles.chair3ImageStyles}
                        source={require('../../assets/Chair3.jpg')}
                    />

                    <Image style={styles.chair1ImageStyles}
                        source={require('../../assets/Chair1.jpg')}
                    />

                </View>

                <Text style={styles.subText2Styles}>Wooden Chair</Text>

                <Text style={styles.subText3Styles}>
                  
                    This is has very soft material and a modern
                    design that makes you feel happy. A seat of
                    office authority such as that of a bishop.
                    
                </Text>

                <TouchableOpacity style={{ marginLeft: 12, marginTop: 100 }}>

                    <View style={styles.button1Styles}>
                        <Text style={styles.button1TextStyle}>Cart</Text>
                    </View>

                    <View style={styles.button2Styles}>
                        <Text style={styles.button2TextStyles}>Buy</Text>
                    </View>

                </TouchableOpacity>

            </View >
        )
    }
}

